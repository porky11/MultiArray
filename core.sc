using import Array
using import utils.iteration
using import utils.utils
using import utils.va
using import utils.functional
using import utils.transitive

let MultiArray =
    typename "MultiArray"

typefn MultiArray 'with-array-type (array-type args...)
    fn (types...)
        let type =
            typename
                .. "<MultiArray "
                    string-repr (array-type (tuple) args...)
                    .. " " (va-map (compose (curry (do ..) " ") string-repr) types...)
                    ">"
                super = MultiArray
                storage =
                    tuple (va-map (compose unknownof (fn (type) (array-type type args...))) types...)
        set-type-symbol! type 'TypeCount (va-countof types...)
        set-type-symbol! type 'TypeTuple (tupleof types...)
        type


typefn MultiArray 'with-memory (memory types...)
    (MultiArray.with-array-type Array (memory = memory)) types...


typefn MultiArray '__typecall (self types...)
    assert (self == MultiArray) "Subtypes of MultiArray have to be allocated"
    self.with-memory HeapMemory types...

typefn& MultiArray '__new (array)
    let AT =
        typeof& array
    for i in (unroll-range AT.TypeCount)
        construct
            ((storage& array) @ i)

#typefn& MultiArray '__typeattr (array symbol)
    @ (storage& array) (va-pos arg (unpack T.TypeTuple)) ...

typefn& MultiArray '__@ (array arg ...)
    let T = (typeof& array)
    if (integer? arg)
        @ (storage& array) arg ...
    elseif (type? arg)
        @ (storage& array) (va-pos arg (unpack T.TypeTuple)) ...

#typefn& MultiArray 'typematch (array type index method args...)
    
        @ (storage& array) (va-pos arg (unpack T.TypeTuple))


typefn& MultiArray 'append (array value)
    let AT =
        typeof& array
    let value =
        deref value
    let T =
        typeof value
    'append (array @ T) value

typefn& MultiArray 'emplace-append (array T ...)
    'emplace-append (array @ T) ...

typefn Closure 'call-value (self types...)
    self types...

typefn Symbol 'call-value (self type ...)
    assert (va-empty? ...) "symbol call values only supported for zero multimap count"
    let value =
        forward-typeattr (ref (pointer type)) self
    value

fn iter-arrays (arg-count method args...)
    fn (arrays...)
        fn (counts...)
            assert ((va-countof arrays...) == (va-countof counts...)) "This should never appear"
            if (va-empty? arrays...)
                method args...
            else
                let array arrays... = arrays...
                let count counts... = counts...
                iterate count array arg-count
                    fn (args...)
                        ((iter-arrays arg-count method args...) arrays...) counts...
                    args...

fn multimap-array (AT array count method arg-count args...)
    iter-tuples-include-self count (unroll-range AT.TypeCount)
        fn (ids...)
            let callable =
                'call-value method (va-map (curry (do @) AT.TypeTuple) ids...)
            if (not (none? callable))

                let counts... =
                    va-group-counts ids...
                
                let ids... =
                    va-remove-doubles ids...
                let arrays... =
                    va-map (curry (do @) array) ids...
                ((iter-arrays arg-count callable args...) (va-reverse arrays...)) (va-reverse counts...)

typefn& MultiArray 'map (array ...)
    let count method args... =
        if (integer? ...)
            ...
        else
            _ 1 ...
    multimap-array (typeof& array) array count method 1 args...

typefn& MultiArray 'index-map (array ...)
    let count method args... =
        if (integer? ...)
            ...
        else
            _ 1 ...
    multimap-array (typeof& array) array count method 2 args...

typefn& MultiArray 'dispatch (array T index f args...)
    let AT =
        typeof& array
    for i in (unroll-range AT.TypeCount)
        let type =
            AT.TypeTuple @ i
        if (type == T)
            let value =
                @ array i index
            f value args...


locals;

