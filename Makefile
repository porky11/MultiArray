.RECIPEPREFIX+= 

SCOPES_INSTALL_DIRECTORY=/usr/local/lib/scopes/

.PHONY: check install test

check: core.sc
    scopes core.sc

test: core.sc testing/test.sc
    scopes testing/test.sc

install:
    rm $(SCOPES_INSTALL_DIRECTORY)/MultiArray.sc
    ln -s $(shell pwd)/core.sc $(SCOPES_INSTALL_DIRECTORY)/MultiArray.sc

