using import ..core
using import utils.va

fn print-equality (T...)
    if (== (va-map bitcountof T...))
        fn (...)
            print "all same types" ...
    elseif (< (va-map bitcountof T...))
        fn (...)
            print "all different types" ...
let AT =
    MultiArray u8 u16 u32 u64
let array =
    local AT
'append array 1:u8
'append array 2:u8
'append array 3:u8
'append array 1:u16
'append array 2:u16
'append array 3:u16
'append array 1:u32
'append array 2:u32
'append array 3:u32
'append array 1:u64
'append array 2:u64
'append array 3:u64

let additional-args... = 1 2 3
'map array 3 print-equality additional-args...


