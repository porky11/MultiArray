using import ..core

struct Pos
    pos : f32
    method& 'update (self)
        print "updating Pos"
        true

struct Move
    pos : f32
    vel : f32
    method& 'accelerate (self acc)
        print "accelerating Move"
        self.vel += acc
        true

    method& 'update (self)
        print "updating Move"
        self.pos += self.vel
        true

let PosArrayType =
    MultiArray Pos Move

locals;
