using import ..core
using import .types

fn set-pos (array T index value)
    'dispatch array T index
        fn (self)
            self.pos = value

let pos-array =
    local PosArrayType

'emplace-append pos-array Pos
    pos = 1.0

'emplace-append pos-array Move
    pos = 0.0
    vel = 1.0


let T index = Pos 0

set-pos pos-array T index 2.0

let val = (pos-array @ T @ index)
print "value is now" val.pos
assert (val.pos == 2.0)

let T* =
    (alloca type) as ref

T* = Pos

let index* =
    local usize 0:usize

set-pos pos-array (load T*) index* 3.0

let val = (pos-array @ T @ index)
print "value is now" val.pos
assert (val.pos == 3.0)

