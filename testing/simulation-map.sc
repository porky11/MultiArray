using import ..core
using import .types

let pos-array =
    local PosArrayType

'emplace-append pos-array Move
    pos = 1.0
    vel = 2.0

'emplace-append pos-array Move
    pos = 2.0
    vel = 3.0

'emplace-append pos-array Move
    pos = 3.0
    vel = 4.0

'append pos-array
    Pos
        pos = 3.0
'emplace-append pos-array Pos
    pos = 4.0

fn collide (X Y)
    if (X == Move or Y == Move) #collision detection is not needed for static objects
        fn (a b accuracy)
            if ((abs (b.pos - a.pos)) <= accuracy)
                print "collision detected"

'map pos-array 'accelerate 0.5

'map pos-array 1 'update

'map pos-array 2 collide 0.5


